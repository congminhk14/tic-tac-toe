import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from 'src/app/services/game.service';
import { User } from '../../models/user.model';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  currentUser: User;

  constructor(protected router: Router,
    protected authenticationService: AuthenticationService,
    public gameService: GameService
    ) {
    this.authenticationService.currentUser.subscribe(res => this.currentUser = res);
  }

  ngOnInit(): void {
  }

  logout() {
    this.authenticationService.logout();
    this.gameService.newGame('bot');
    this.router.navigate(['/login']);
  }

  resetGame() {
    this.gameService.botMode ? this.gameService.newGame('bot') : this.gameService.newGame('friend');
    
  }

  selectGameMode(mode: string) {
    this.gameService.newGame(mode);
  }

}
