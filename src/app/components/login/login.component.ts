import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { ApiService } from '../../services/api.service';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });
  submitted = false;
  returnUrl: string;
  userList = [];
  isValid = false;
  loading = false

  constructor(
    protected formBuilder: FormBuilder,
    protected route: ActivatedRoute,
    protected router: Router,
    protected authenticationService: AuthenticationService,
    protected alertService: AlertService,
    protected apiService: ApiService
  ) {}

  ngOnInit() {
    this.getUserData();
  }

  getUserData() {
    this.apiService.getUser().subscribe(res => {
      this.userList = res;
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    for (let i = 0; i < this.userList.length; i++) {
      if (this.f.username.value !== this.userList[i].username) {
        return this.isValid = true;
      } else if (this.f.password.value == this.userList[i].password) {
        this.loading = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value);
      } else {
        this.isValid = true;
      }
    }
  }
}
