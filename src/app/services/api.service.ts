import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  constructor(private http: HttpClient) {}

  getUser() {
    return this.http.get<User[]>('https://sheet2api.com/v1/gcI9aGQTZ6Yd/user');
  }
}
