import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})

export class GameService {

  public board = [];
  boardSize: number = 9;
  activePlayer: string = "X";
  turnCount: 0;
  isGameRunning: boolean = false;
  isGameOver: boolean = false;
  winner: boolean = false;
  tie: boolean = false;
  computerPlay: boolean = false;
  botMode: boolean = true;

  constructor() {
    this.newGame('bot');
  }

  newGame(mode: string) {
    this.activePlayer = "X";
    this.turnCount = 0;
    this.isGameRunning = false;
    this.isGameOver = false;
    this.winner = false;
    this.board = this.createBoard();
    this.tie = false;
    this.computerPlay = false;
    this.botMode = (mode === 'bot') ? true : false;
  }

  createBoard() {
    let board = [];
    for (let i = 0; i < 9; i++) {
      board.push({ id: i, state: null })
    };
    return board
  }

  get getBoard() {
    return this.board
  }

  set setBoard(board) {
    this.board = [...board]
  }

  changePlayerTurn(squareClicked) {
    this.updateBoard(squareClicked)
    if (!this.isGameOver) {
      this.activePlayer = this.activePlayer === "X" ? "O" : "X";
      this.computerPlay = this.computerPlay ? false : true;
    }
    this.turnCount++;
    this.isGameOver = this.isGameOver ? true : false;

    if (this.botMode && this.computerPlay && this.turnCount < 9) {
      this.handleComputerPlay();
    }
    
    if (this.turnCount === 9 && !this.isGameOver) {
      this.checkTie();
    }
  }

  handleComputerPlay() {
    let botPlayBoard = [];
    for (let i = 0; i < this.board.length; i++) {
      if (this.board[i].state === null) {
        botPlayBoard.push(this.board[i]);
      }
    }
    const item = botPlayBoard[Math.floor(Math.random() * botPlayBoard.length)];
    
    item.state = this.activePlayer;
    this.changePlayerTurn(item);
  }

  checkTie() {
    this.tie = true;
    this.isGameRunning = false;
    this.isGameOver = true;
  }

  updateBoard(squareClicked) {
    this.board[squareClicked.id].state = squareClicked.state;
    if (this.isWinner) {
      this.winner = true;
      this.isGameRunning = false;
      this.isGameOver = true;
    }
  }

  get gameOver(): boolean {
    return this.turnCount > 8 || this.winner ? true : false;
  }

  get isWinner(): boolean {
    return this.checkDiag() || this.checkRows(this.board, "row") || this.checkRows(this.board, "col") ? true : false;
  }

  checkRows(board, mode): boolean {

    const
      ROW = mode === "row" ? true : false,
      DIST = ROW ? 1 : 3,
      INC = ROW ? 3 : 1,
      NUMTIMES = ROW ? 7 : 3;

    for (let i = 0; i < NUMTIMES; i += INC) {

      let
        firstSquare = board[i].state,
        secondSquare = board[i + DIST].state,
        thirdSquare = board[i + (DIST * 2)].state;

      if (firstSquare && secondSquare && thirdSquare) {
        if (firstSquare === secondSquare && secondSquare === thirdSquare) return true
      }
    }
    return false
  }

  checkDiag() {
    const timesRun = 2,
      midSquare = this.board[4].state;

    for (let i = 0; i <= timesRun; i += 2) {

      let
        upperCorner = this.board[i].state,
        lowerCorner = this.board[8 - i].state;

      if (midSquare && upperCorner && lowerCorner) {
        if (midSquare === upperCorner && upperCorner === lowerCorner) return true
      }
    }

    return false
  }

}